/**
 * CS145 - Homework #7: QuestionNode class
 * This node class is intended to be used in a BinaryTree format. It stores either a question or an answer as a String and a flag is set
 * to identify whether it is a question or an answer "node."
 * @author Thomas Helms
 * 
 * 
 *
 */
public class QuestionNode
{
	//fields
	QuestionNode left;
	QuestionNode right;
	QuestionNode previous;
	boolean isQuestion;
	String data;
	
	//constructor
	public QuestionNode(QuestionNode previous, String data, boolean isQuestion)
	{
		this.previous = previous;
		this.data = data;
		this.isQuestion = isQuestion;
	}
	
	//getters
	
	/**
	 * 
	 * @return True if the node is a question or false if it is an answer.
	 */
	public boolean isQuestion()
	{
		return isQuestion;
	}
	
	/**(Note: This method is just an inverted return value of isQuestion. However, it will help identify clearer code in higher code.)
	 * 
	 * @return True if the node is an answer or false if it is a question.
	 */
	public boolean isAnswer()
	{
		return !isQuestion;
	}
	
}