/**
 * CS145 Homework #7 - TwentyQuestions
 * This program is designed to sort a list of question and answers from a file and play a game of Twenty Questions with the user.
 * The game will also learn from the user if it loses and will write back to a file with the learned nodes.
 * The tree is a Binary Tree type, and it is not limited to the typical 20 questions that the game implies.
 * 
 * @author Thomas Helms
 */
import java.util.*;
import java.io.*;
public class QuestionTree
{
	//fields
	private UserInterface ui;
	private int totalGames;
	private int gamesWon;
	private QuestionNode root;
	
	//constructor
	public QuestionTree(UserInterface ui)
	{
		if (ui == null)
			throw new IllegalArgumentException();
		
		this.ui = ui;
		totalGames = 0;
		gamesWon = 0;
		
	}
	/**
	 * This method recursively goes through the node tree and plays until it loses or wins.
	 * @param select The current QuestionNode that has been traversed through in the tree.
	 * @return The QuestionNode that has been modified (or not modified) during the iteration. (Used for x=change(x)).
	 */
	private QuestionNode recursivePlay(QuestionNode select)
	{		
		if (select.isAnswer())
		{
			ui.print("Would your object happen to be " + select.data + "? ");
			if (ui.nextBoolean())
			{
				ui.print("I win!");
				gamesWon++;
				return select;
			}
			select = restructure(select); //Add a question to this location and push the existing node down one level.
			return select;
		}
		
		else //if this is a question
		{
			ui.print(select.data);
			boolean userAnswer = ui.nextBoolean();
			if (userAnswer) //if yes
			{	
				select.left = recursivePlay(select.left);
			}
			else //if no
			{
				if (select.right != null)
				{
					select.right = recursivePlay(select.right);
				}
			}				
		}
			return select;
				
		
	}
	/* Public method used to kickstart recursivePlay() by sending in the root field.
	 * The 'root' field of this QuestionTree object is modified when recursivePlay() returns all the way back to here.
	 */
	public void play()
	{
		totalGames++;
		this.root = recursivePlay(this.root);
	}	
	/**
	 * 
	 * @return The total amount of games played within this object.
	 */
	public int totalGames()
	{
		return this.totalGames;
	}
	/**
	 * 
	 * @return The total amount of games won within this object.
	 */
	public int gamesWon()
	{
		return this.gamesWon;
	}
	
	/**
	 * This method was factored-out code from play() in order to clean up the "losing" process.
	 * @param select QuestionNode sent in that the computer lost on.
	 * @return The modified QuestionNode structure that has two new nodes inserted. (The extra answer and the new question.)
	 */
	private QuestionNode restructure(QuestionNode select)
	{
		ui.print("I lose. What is your object?");
		String userObject = ui.nextLine();
		ui.print("Type a yes/no question to distinguish your item from " + select.data +":");
		String userQuestion = ui.nextLine();
		ui.print("And what is the answer for your object?");
		boolean userAnswer = ui.nextBoolean();
					
					//node shift and add subroutine
				QuestionNode node = new QuestionNode(select.previous, userQuestion, true);
				
				if (userAnswer) //yes
				{
					node.left = new QuestionNode(select, userObject, false);
					node.right = select;
				}
				else if (!(userAnswer)) //no
				{
					node.right = new QuestionNode(select, userObject, false);
					node.left = select;
				}
		return node;
	}
	/**
	 * This method verifies that the incoming Scanner is valid and kickstarts recursiveLoad().
	 * @param input Incoming scanner from client code.
	 */
	public void load(Scanner input)
	{
		if (input == null || !(input.hasNextLine()))
			throw new IllegalArgumentException();
					
		String temp = input.nextLine();
		root = new QuestionNode(null, temp.substring(2), true);
		
		//This block shouldn't be needed, but Scanner is throwing a strange error that shouldn't happen with the .hasNextLine() checks...
		try{
		//initalize the topmost QuestionNode
			recursiveLoad(input, root);
		}
		catch (NoSuchElementException sc)
		{
			//temp fix for NoSuchElementException thrown from scanner
			
			
		}
	}
	/**
	 * This method is public-facing and kickstarts recursiveSave() by passing in the root field.
	 * @param output
	 */
	public void save(PrintStream output)
	{
		recursiveSave(output, this.root);
	}
	
	/**
	 * This method recursively navigates through the QuestionNode binary tree and saves both question and answers to a PrintStream.
	 * @param output The PrintStream directed towards a file provided by the client code.
	 * @param select The current QuestionNode that is being traversed.
	 */
	private void recursiveSave(PrintStream output, QuestionNode select)
	{
		if (select == null)
			return;
			
		else
		{
			if (select.isQuestion())
			{
				output.println("Q:"+select.data);
			}
			else
			{
				output.println("A:"+select.data);
			}
		}
		
		recursiveSave(output, select.left);
		recursiveSave(output, select.right);
	}
/**
 * This method recursively navigates through the provided Scanner and adds them back into a QuestionNode binary tree.	
 * @param input Scanner provided by client code that (typically) hooks onto a previously generated file.
 * @param incoming The current QuestionNode that has been traversed in this recursive method.
 */
	private void recursiveLoad(Scanner input, QuestionNode incoming)
	{	//if scanner is empty, stop.
		if (!input.hasNextLine())
			return;
		//used as a check for if we have more than 2 answers in a row
		if (incoming.left != null && incoming.right != null)
				recursiveLoad(input, incoming.previous);
			
			String temp = input.nextLine();
			
			if (temp.startsWith("Q:"))
			{
				if (incoming.left == null)
				{
					incoming.left = new QuestionNode(incoming, temp.substring(2),true);
					recursiveLoad(input,incoming.left);
				}
				
				else if (incoming.right == null)
				{
					incoming.right = new QuestionNode(incoming, temp.substring(2),true);
					recursiveLoad(input,incoming.right);
				}
	
			}
			
			else if (temp.startsWith("A:"))
			{
				if (incoming.left == null)
				{
					incoming.left = new QuestionNode(incoming, temp.substring(2),false);
					recursiveLoad(input,incoming); 
				}
				else if (incoming.right == null)
				{
					incoming.right = new QuestionNode(incoming, temp.substring(2),false);
					recursiveLoad(input,incoming);
				}
			}	
		
	}
}